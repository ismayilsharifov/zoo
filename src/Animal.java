import java.util.Random;

public abstract class Animal {
	//fields
	private int lifeExpectancy;
	private String[] eats;
	private int age;
	private char gender;
	private int health;
	private Enclosure enclosure;
	private int waste;
	
	protected StringBuilder animalDetails;

	//Static fields
	public static final int MAX_health = 10;
	public static final int MIN_health = 0;
	//animal -in en kichik oldugu 1 ayliq dovrunu initial goturmek chunki yashi 0 ola bilmez
	public static final int startingAge = 1;

	public Animal(int lifeExpectancy, String[] eats, char gender, int initialAge, int initialHealth){
		this.age = initialAge;
		setHealth(initialHealth);
		this.lifeExpectancy = lifeExpectancy;
		setGender(gender);
		this.eats = eats;
		this.waste = 0;
		animalDetails = new StringBuilder();
	}

	//NoArg constructor
	public Animal(){
		this.age = startingAge;
		this.health = MAX_health;
		this.lifeExpectancy = 30;
		setGender(gender);
		this.eats = new String[] {"hay","fruit","steak","celery"};
		this.waste = 0;
		animalDetails = new StringBuilder();
	}

	//set && get Gender
	private void setGender(char gender){
		if (gender == 'f'){
			this.gender = gender;
		} else {
			this.gender = 'm';
		}
	}
	public char getGender(){
		return gender;
	}

	//setHealth funksiyasi sadece constructorda ve increase,decrease funksiyalarinda istifade oluna bilenecek
	// dolayisila animal clasina mexsusdu ve private olacaq
	private void setHealth(int healthPoints){
		health = healthPoints;
		if (health > MAX_health){
			health = MAX_health;
		} else if (health < MIN_health) {
			health = 0;
		}
	}
	public boolean decreaseHealth(int health1){
		if (health1 > 0){
			animalDetails.append("looses " + health1 + " health -> ");
			setHealth(health - health1);
			return true;
		} else {
			return false;
		}
	}
	public void decreaseHealth(){
		decreaseHealth(1);
	}

	public boolean increaseHealth(int healthPoints){
		if (healthPoints > 0){
			animalDetails.append("gains " + healthPoints + " health; ");
			setHealth(health + healthPoints);
			return true;
		} else {
			return false;
		}
	}

	public void increaseHealth(){
		increaseHealth(1);
	}

	public int getAge(){
		return age;
	}

	private boolean isDead(){
		if (health <= MIN_health){
			return true;
		}if (age >= lifeExpectancy){
			return true;
		}
		return false;
	}

	public boolean aMonthPasses(){
		animalDetails.setLength(0);
		eat();
		excrete();
		decreaseHealth(2);
		increaseAge();
		if (isDead()){
			animalDetails.append("dies. ");
			return false;
		}
		return true;
	}

	public boolean canEat(String food){
		int i = 0;
		while ( i < eats.length ){
			if (eats[i].equals(food)){
				return true;
			}i++;
		}
		return false;
	}

	public boolean eat(){
		int foodCount = eats.length;
		String food = "";
		boolean eaten = false;
		while ((foodCount > 0) && (!eaten)) {
			foodCount--;
			food = eats[foodCount];
			eaten = enclosure.getFoodstore().takeFood(food);
		}if (eaten){
			animalDetails.append("eats " + food + " -> ");
			increaseHealth( Foodstore.getFoodHealth(food) );
			waste = waste + Foodstore.getFoodWaste(food);
			return true;
		} else{
			animalDetails.append("FAILS to eat. ");
			return false;
		}
	}
	


	public String[] getEatsArray(){
		return eats;
	}

	public void setEnclosure(Enclosure enclosure){
		this.enclosure = enclosure;
	}

	public void increaseAge(){
		animalDetails.append("gets older by " + 1 + " month -> ");
		age++;
	}

	public void excrete(){
		enclosure.addWaste(waste);
		animalDetails.append("excrets " + waste + " waste -> ");
		waste = 0;
	}

	public abstract void treat();

	public StringBuilder getAnimalDetails(){
		return new StringBuilder(this.getClass().getName() + "("+ gender +") "
				+" " + animalDetails + "\n\t\t"
				+ " at the end of month has " + health + " health and is " + age + " months old ;");
	}
}