import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class Enclosure {

	private ArrayList<Animal> animals;         //her qefesde heyvanlar
	private Foodstore foodstore;              //her qefesde qidalar
	private int waste;                       //her qefesde tullanti
	private int enclosureID;                //qefes unikal identifikasiya
	private StringBuilder enclosureDetails;
	//no-arg konstruktor

	public Enclosure() {
		animals = new ArrayList<Animal>();
		foodstore = new Foodstore();
		enclosureDetails = new StringBuilder();
		waste = 0;
	}

	//enclouseri id-e gore set get metodlari
	public int getEnclosureID() {
		return enclosureID;
	}

	public void setEnclosureID(int iD) {
		enclosureID = iD;
	}

	public void addWaste(int quantity) {
		waste += quantity;
	}

	//tullanti sayi
	public int getWasteSize() {
		return waste;
	}

	//qefesdeki foodstore
	public Foodstore getFoodstore() {
		return foodstore;
	}

	public int size() {
		return animals.size();
	}

	public boolean isEmpty() {
		return (size() == 0);
	}

	//qefesee sayi 20den chox olmamaq sherti ile heyvan elave etmek
	public boolean addAnimal(Animal animal) {
		if ( this.size() < 20) {
			animals.add(animal);
			animal.setEnclosure(this);
			enclosureDetails.append("animal added to enclosure; ");
			return true;
		} else {
			enclosureDetails.append("enclosure full; ");
			return false;
		}
	}
	//sechilmish heyvani qefesden chixardiqda bundan qefes classi da,heyvan classi da xeberdar olmalidir
	public void removeAnimal(Animal animal) {
		if (animals.remove(animal)) {
			animal.setEnclosure(null);
		}
	}

	//tullantilari qefesden chixaran metod.
	// Eger parametr sayca tullantidan choxdusa metod tullantini sifirlasin
	public boolean removeWaste(int count) {
		if (count < 0)
			return false;
		else if (count > waste)
			waste = 0;
		else {
			waste -= count;
		}
		return true;
	}

	public boolean aMonthPasses() {
		enclosureDetails.setLength(0);
		boolean animalAlive;
		Iterator<Animal> iterator = animals.iterator();

		// her heyvan uchun cagirib oluleri qefesden chixarmaq
		while (iterator.hasNext()) {
			Animal animal = iterator.next();
			animalAlive = animal.aMonthPasses();
			if (!animalAlive) {
				enclosureDetails.append(animal.getClass().getName() + "(" + animal.getGender() + ") dies at "+ animal.getAge() +"; ");
				iterator.remove();
			}
		}
		// qefes boshdursa false qaytarsin
		return !this.isEmpty();
	}

	//cari qefesdeki heyvanlarin animal clasindaki eats[] arrayina uygun
	//hansi qidani yeye bildiyine uygun qida teleb eden hashmap tertib etmek

	public HashMap<String, Integer> getFoodRequirement() {
		HashMap<String, Integer> foodRequirement = new HashMap<String, Integer>();

		// qefesdeki her heyvan uchun
		for (Animal a : animals) {
			for (String food : a.getEatsArray()) {
				if (!foodRequirement.containsKey(food)) {
					foodRequirement.put(food, 1);
				} else {
					foodRequirement.put(food, foodRequirement.get(food) + 1);
				}
			}
		}
		// eger qefesin foodstorunda lazimi yemek varsa onu yemek teleb siyahisi hashmapindan silirik
		for (String food : foodRequirement.keySet()) {
			foodRequirement.put(food, foodRequirement.get(food) - foodstore.foodAvailable(food));
		}
		return foodRequirement;
	}
	
	public Animal getAnimalAt(int index){
		if ( (index < size()) && (index >= 0) )
			return this.animals.get(index);
		else 
			return null;
	}

	public StringBuilder getEnclosureDetails() {
		StringBuilder status = new StringBuilder("ENCLOSURE "+ getEnclosureID() + ": \t size(" + size() + ")  waste(" + waste + ") \n");
		status.append(foodstore.getFoodStoreDetails());
		status.append("\n");
		status.append( enclosureDetails + "\n");
 		status.append("> ANIMALS: \n");
		for (Animal animal : animals) {
			status.append("\t\t");
			status.append(animal.getAnimalDetails());
			status.append("\n");
		}
		return status;
	}
}
