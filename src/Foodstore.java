import java.util.HashMap;
import java.util.Set;

public class Foodstore {
	//can ve tullanti staticdir chunki classa mexsusdur
	//erzaqlar ve onlarin verdiyi can (classa mexsus static)
	private static HashMap<String, Integer> foodHealth =  new HashMap<String, Integer>() {{
		put("hay", 1);
		put("steak", 4);
		put("fruit", 2);
		put("celery", 0);
		put("fish", 3);
		put("ice cream", 1);
	}};
	//erzaqlar ve onlarin yaratdigi tullanti (classa mexsus static)
	private static HashMap<String, Integer> foodWaste = new HashMap<String, Integer>() {{
		put("hay", 4);
		put("steak", 4);
		put("fruit", 3);
		put("celery", 1);
		put("fish", 2);
		put("ice cream", 3);
	}};
	//klasa mexsus olan metodlar:
	//butun mumkun erzaqlarin siyahisi

	public static Set<String> getFoodSet(){
		return foodHealth.keySet();
	}

	//erzagin nece can verdiyini hesablayir
	public static int getFoodHealth(String food){
		return foodHealth.get(food);
	}

	//erzagin yaratdigi artigi hesablayir
	public static int getFoodWaste(String food){
		return foodWaste.get(food);
	}

	private HashMap<String, Integer> foodQuantity;
	
	public Foodstore(){
		foodQuantity = new HashMap<String, Integer>();
		resetFoodQuantity();
	}
	
	//qida anbarindaki butun qidalari default 0 qiymet verir
	// (FoodHealth ve FoodCount sinxrondur)
	public void resetFoodQuantity(){
		for (String key : Foodstore.foodHealth.keySet()){
			foodQuantity.put(key, 0);
		}
	}

	// erzaqlarin miqdarini cemleyir cem 0 olsa dogrudur
	public boolean isEmpty(){
		int sum = 0;
		for (String food : foodQuantity.keySet()){
			sum += foodQuantity.get(food);
		}if (sum <= 0){
			return true;
		} else{
			return false;
		}
	}

	public void addFood(String food, int count) throws Exception{
		if (!foodHealth.containsKey(food)) {
			throw new Exception(food + " as a type of food does not exist!");
		}
		if (count < 0) {
			throw new Exception("Negative quantity: " + count);
		}
		int iniQuantity = foodQuantity.get(food); // initial quantity
		foodQuantity.put(food, iniQuantity + count);
	}
	public boolean takeFood(String food){
		if (foodQuantity.get(food) <= 0){
			return false;
		} else {
			foodQuantity.put(food, foodQuantity.get(food) - 1);
			return true;
		}
	}
	public int foodAvailable(String food){
		return foodQuantity.get(food);
	}
	
	public StringBuilder getFoodStoreDetails(){
		StringBuilder status = new StringBuilder("> FOODSTORE: ");
		status.append("\t");
		for (String food : foodQuantity.keySet()){
			status.append(" " + food + "(" + foodQuantity.get(food) + ") ");
		}
		return status;
	}
}