import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Zookeeper {

	// zookeeperin baxacagi qefesler
	private ArrayList<Enclosure> enclosures;
	private Zoo zoo;

	//generic zookeper ancaq stroke ve hug ede bilir yeni bigcat ve bear uchun qaygi gostere bilir
	//wildcardla generic ArrayList qurmaq olar ki alt classlarda elave olaraq qaygi gostere bildiyi heyvanlar elave olunsun
	protected ArrayList<Class<? extends Animal>> canTreat = 
			new ArrayList<Class<? extends Animal>>(Arrays.asList(Lion.class, Tiger.class, Bear.class));

	protected StringBuilder zookeperDetails;

	public Zookeeper(Enclosure enclosure) {
		enclosures = new ArrayList<Enclosure>();
		addEnclosure(enclosure);
		this.zoo = null;
		zookeperDetails = new StringBuilder();
	}
	//no-Arg konstruktor
	public Zookeeper() {
		enclosures = new ArrayList<Enclosure>();
		this.zoo = null;
		zookeperDetails = new StringBuilder();
	}

	// indexe gore qaytarir
	public Enclosure getEnclosure(int index) {
		return enclosures.get(index);
	}

	public void addEnclosure(Enclosure enclosure) {
		enclosures.add(enclosure);
	}

	public boolean hasNoEnclosures() {
		return (enclosures.size() == 0);
	}

	public void setZoo(Zoo zoo) {
		this.zoo = zoo;
	}

	public Zoo getZoo() {
		return this.zoo;
	}

	public void aMonthPasses() {
		zookeperDetails.setLength(0);
		removeWaste();
		moveFood();
		giveTreat();
	}
	public void moveFood(Enclosure enclosure) {
		HashMap<String, Integer> requirements = enclosure.getFoodRequirement();

		int foodCount = 20; //sherte esasen max. 20 qida elave ede biler ve bunlarin hansi qidalar olacagi teleb siyahisindan asilidir;

		try {
			boolean requirementMet = (Collections.max(requirements.values()) <= 0);

			boolean foodTanken = true;

			while ((foodCount > 0) && (!requirementMet) && (foodTanken)) {
				foodTanken = false;
				for (String food : requirements.keySet()) {
					if ((foodCount > 0) && (requirements.get(food) > 0) && (zoo.getFoodstore().takeFood(food))) {
						enclosure.getFoodstore().addFood(food, 1);
						//anbara elave olunan qida reseptden chixarilir
						requirements.put(food, requirements.get(food) - 1);
						foodCount--;
						zookeperDetails.append("moves " + 1 + " " + food + " to enclosure " + enclosure.getEnclosureID() + " -> ");
						foodTanken = true;
					}
				}
				requirementMet = (Collections.max(requirements.values()) <= 0);
			}

		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}
	public void moveFood(){
		for (Enclosure enclosure : enclosures){
			if (!enclosure.isEmpty()) {
				moveFood(enclosure);
			}
		}
	}
	public void removeWaste(Enclosure enclosure) {
		int removeMAX = 20;
		int wasteRemove = enclosure.getWasteSize();

		zookeperDetails.append("removes " );
		if ( wasteRemove > removeMAX ) {
			zookeperDetails.append( (enclosure.removeWaste(removeMAX)? removeMAX : 0 ));
		} else {
			zookeperDetails.append( (enclosure.removeWaste(wasteRemove)? wasteRemove : 0 ));
		}
		zookeperDetails.append(" waste from Enclosure " + enclosure.getEnclosureID() + " -> ");
	}
	public void removeWaste(){
		for (Enclosure enclosure : enclosures){
			removeWaste(enclosure);
		}
	}
	public void giveTreat(Enclosure enclosure) {
		Animal animal;
		int i = 0;
		int toTreat = 2;
		while ( (i < enclosure.size()) && (toTreat > 0) ) {
			animal = enclosure.getAnimalAt(i);
			if (canTreat.contains(animal.getClass())){
				zookeperDetails.append(" treats " + animal.getClass().getName()+ " in enclosure " + enclosure.getEnclosureID() + " | ");
				animal.treat();
				toTreat--;
			}
			i++;
		}
	}
	public void giveTreat(){
		for (Enclosure enclosure : enclosures){
			giveTreat(enclosure);
		}
	}
	public StringBuilder getDetailStatus(){
		return new StringBuilder("> ZOOKEEPER (" + this.getClass().getName() + ") : " + zookeperDetails);
	}
}