import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Simulation {

	private static ArrayList<Zoo> simZoos = new ArrayList<Zoo>();
	private static int zooCount = 0;
	private static Zoo lastZooAdded;

	public static void main(String[] args) {
		String filename = "example.txt";
		runSimulation(filename);
	}
	
	public static void runSimulation(String filename ) {
		try {
			loadFile(filename);
			boolean zoosRunning = true;
			int month = 1;
	
			// her zoo uchun aMonthPasses cagirir
			while (zoosRunning) {
				zoosRunning = false;
				System.out.println();
				System.out.print("\t\t\t\t\t**-------***--------*****--------***---------**\n");
				System.out.print("\t\t\t\t\t*                  Month " + month + " ...                *\n");
				System.out.print("\t\t\t\t\t**-------***--------*****--------***--------***\n");
				for (Zoo zoo : simZoos) {
					if (zoo.aMonthPasses()) {
						System.out.println(zoo.getZooDetails());
						zoosRunning = true;
					}
				}try{
					Thread.sleep(500);
				} catch (InterruptedException e) {
					System.err.println("ERROR!");
					System.err.println(e.getMessage());
				}
				month++;
			}
			for (Zoo zoo : simZoos) {
				System.out.println(zoo.getZooDetails());
				zoosRunning = true;
			}
		}catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}

		System.out.println("\n\t\t\t\t\tNo zoos are running, simulation has ended!");
	}

	public static void loadFile(String filename) throws Exception{
		String line = null;
		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		while ((line = bufferedReader.readLine()) != null) {
			processingAdd(line);
		}
		bufferedReader.close();
	}

	public static void processingAdd(String line) throws Exception {
		String keyWord = line.split(":")[0];
		if (keyWord.equals("zoo")) {
			Zoo zoo = new Zoo();
			addZoo(zoo);
			lastZooAdded = zoo;
		}else if (lastZooAdded != null) {
			if (keyWord.equals("enclosure")) {
				Enclosure enclosure = new Enclosure();
				enclosure.addWaste(Integer.parseInt(line.split(":")[1]));
				lastZooAdded.addEnclosure(enclosure);
			}else if (lastZooAdded.lastEnclosureAdded() == null) {

				if (Foodstore.getFoodSet().contains(keyWord)) {
					int quantity = Integer.valueOf(line.split(":")[1]);
					lastZooAdded.getFoodstore().addFood(keyWord, quantity);
				}
			}else if (lastZooAdded.lastEnclosureAdded() != null) {
				Enclosure lastEnclosure = lastZooAdded.lastEnclosureAdded();
				// qefesdeki anbar
				if (Foodstore.getFoodSet().contains(keyWord)) {
					int quantity = Integer.valueOf(line.split(":")[1]);
					lastEnclosure.getFoodstore().addFood(keyWord, quantity);
				}else {
					if ( Zoo.animalTypes.contains(keyWord.toLowerCase()) ) {
						addAnimal(keyWord, line.split(":")[1]);
					} else if (Zoo.zookeeperTypes.contains(keyWord.toLowerCase())){
						if ( line.split(":").length > 1){ 
							addZookeeper(keyWord, line.split(":")[1]);
						} else {
							addZookeeper(keyWord,"");
						}
					}
				}
			}  else {
				throw new NullPointerException(" No definition for : " + keyWord );
			}
		}
	}

	public static void addZookeeper(String zookeeperType, String details) throws Exception{		
		switch (zookeeperType){
		case "zookeeper":
			Zookeeper zookeeper = new Zookeeper();
			lastZooAdded.addZookeeper(zookeeper);
			break;
		case "physioZookeeper":
			Zookeeper physioZookeeper = new PhysioZookeeper();
			lastZooAdded.addZookeeper(physioZookeeper);
			break;
		case "playZookeeper":
			Zookeeper playZookeeper = new PlayZookeeper();
			lastZooAdded.addZookeeper(playZookeeper);
			break;
		}
		if (details.length() > 0) {
			for (String enclosureID : details.split(",")){
				lastZooAdded.lastZookeeperAdded().addEnclosure(lastZooAdded.getEnclosure(Integer.parseInt(enclosureID)));
			}
		} else {
			lastZooAdded.lastZookeeperAdded().addEnclosure(lastZooAdded.lastEnclosureAdded());
		}
	}

	public static void addAnimal(String animalType, String details) throws Exception{
		char gender = Character.toLowerCase(details.charAt(0));
		int age = Integer.parseInt( details.split(",")[1] );
		int health = Integer.parseInt( details.split(",")[2] );
		Enclosure enclosure;
		if (details.split(",").length == 4) {
			enclosure = lastZooAdded.getEnclosure( Integer.parseInt( details.split(",")[3] ) );
		} else {
			enclosure = lastZooAdded.lastEnclosureAdded();
		}
		switch (animalType) {
		case "lion":
			Lion lion = new Lion(gender, age, health);
			enclosure.addAnimal(lion);
			break;
		case "bear":
			Bear bear = new Bear(gender, age, health);
			enclosure.addAnimal(bear);
			break;
		case "chimpanzee":
			Chimpanzee chimpanzee = new Chimpanzee(gender, age, health);
			enclosure.addAnimal(chimpanzee);
			break;
		case "elephant":
			Elephant elephant = new Elephant(gender, age, health);
			enclosure.addAnimal(elephant);
			break;
		case "gorilla":
			Gorilla gorilla = new Gorilla(gender, age, health);
			enclosure.addAnimal(gorilla);
			break;
		case "penguin":
			Penguin penguin = new Penguin(gender, age, health);
			enclosure.addAnimal(penguin);
			break;
		case "tiger":
			Tiger tiger = new Tiger(gender, age, health);
			enclosure.addAnimal(tiger);
			break;
		case "giraffe":
			Giraffe giraffe = new Giraffe(gender, age, health);
			enclosure.addAnimal(giraffe);
			break;
		}
	}

	public static void addZoo(Zoo zoo){
		simZoos.add(zoo);
		zooCount++;
		zoo.setZooID(zooCount);
	}
}