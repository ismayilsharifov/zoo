  import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zoo {
	
	public static final Set<String> animalTypes = new HashSet<String>(Arrays.asList("lion", "chimpanzee", "tiger", "gorilla", "penguin", "bear", "elephant", "giraffe"));
	public static final Set<String> zookeeperTypes = new HashSet<String>(Arrays.asList("zookeeper", "playzookeeper", "phisiozookeeper"));
	
	private ArrayList<Enclosure> enclosures;
	private ArrayList<Zookeeper> zookeepers;
	private Foodstore foodstore;
	private Enclosure lastEnclosureAdded;
	private Zookeeper lastZookeeperAdded;
	private int zooID;
	private int enclosureCount;
	private boolean zooRun;
	
	public Zoo() {
		enclosures = new ArrayList<Enclosure>();
		zookeepers = new ArrayList<Zookeeper>();
		foodstore = new Foodstore();
		lastZookeeperAdded = null;
		lastEnclosureAdded = null;
		enclosureCount = 0;
		zooRun = true;
	}
	//aMonthPasses metodunu ishledib qefesler ve zookeperler uchun 1 ay ireli
	//metoddan sonra enclosure classina esasen sag qalan heyvanlar qayidir
	public boolean aMonthPasses(){
		zooRun = false;
		for (Enclosure e : enclosures) {
			if (e.aMonthPasses()) {
				zooRun = true;
			}
		}if (zooRun) {
			for (Zookeeper zk : zookeepers) {
				zk.aMonthPasses();
			}try {
				restockFoodstore();
			}catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
		return zooRun;
	}

	public Foodstore getFoodstore() {
		return foodstore;
	}

	//foodstore u default qiymetlere resetlemek
	public void restockFoodstore() throws Exception{
		for (String food : Foodstore.getFoodSet()) {
			foodstore.addFood(food, 50);
		}
	}
	
	public Enclosure getEnclosure(int iD) throws NullPointerException{
		if ((enclosures.size() > iD) && (0 <= iD) ) {
			return enclosures.get(iD);
		} else {
			throw new NullPointerException("Enclosure does not exist.");
		}
	}

	public void addEnclosure(Enclosure enclosure) {
		enclosures.add(enclosure);
		enclosureCount++;
		enclosure.setEnclosureID(enclosureCount);
		this.lastEnclosureAdded = enclosure;
	}

	public Enclosure lastEnclosureAdded() {
		return this.lastEnclosureAdded;
	}

	public void addZookeeper(Zookeeper zookeeper) {
		zookeepers.add(zookeeper);
		this.lastZookeeperAdded = zookeeper;
		zookeeper.setZoo(this);
	}

	public Zookeeper lastZookeeperAdded() {
		return this.lastZookeeperAdded;
	}

	public boolean hasNoEnclosures() {
		return (enclosures.size() == 0);
	}


	public int getZooID() {
		return zooID;
	}

	public void setZooID(int zooID) {
		this.zooID = zooID;
	}
	
	public void go(){
		int month = 0;
		
		while (aMonthPasses()) {
			System.out.println();
			System.out.print("\t\t\t\t\t**-------***--------*****--------***---------**\n");
			System.out.print("\t\t\t\t\t*                  Month " + month + " ...                *\n");
			System.out.print("\t\t\t\t\t**-------***--------*****--------***--------***\n");
			System.out.println(getZooDetails());

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.err.println("ERROR!");
				System.err.println(e.getMessage());
			}
			month++;
		}
		
		System.out.println("\n\t\t\t\t\tNo zoos are running, simulation has ended!");
	}
	public StringBuilder getZooDetails() {
		StringBuilder status = new StringBuilder("\t\t\t\t\t\t\t\t*** ZOO " + this.getZooID() + " : ");
		if (zooRun){
			status.append("RunninG *** \n");

				status.append("\n");
				status.append("> ZOO " + foodstore.getFoodStoreDetails());
					status.append("\n");
					for (Zookeeper zookeeper : zookeepers){
						status.append("\n");
						status.append( zookeeper.getDetailStatus() );
					}
				status.append("\n");

				for (Enclosure enclosure : enclosures) {
					status.append("\n");
					status.append("> " + enclosure.getEnclosureDetails());
				}
		} else {
			status.append("STOPPED!");
		}
		return status;
	}

}